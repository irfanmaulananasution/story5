from django.db import models
from django.utils import timezone
import datetime

# Create your models here.

class Schedule(models.Model):
	time = models.DateTimeField(default=datetime.datetime.now)
	eventsName = models.CharField(max_length=64)
	location = models.CharField(max_length=64)
	category = models.CharField(max_length=32)
	
	def __str__(self):
		return self.activity
