# Generated by Django 2.2.6 on 2019-10-08 01:18

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField(default=datetime.datetime(2019, 10, 8, 1, 18, 46, 987440, tzinfo=utc))),
                ('activityName', models.CharField(max_length=64)),
                ('location', models.CharField(max_length=64)),
                ('category', models.CharField(max_length=16)),
            ],
        ),
    ]
