from django import forms
from .models import Schedule

class addSchedule(forms.ModelForm):
	time = forms.DateTimeField(
		input_formats=['%d/%m/%Y %H:%M'],
		widget=forms.DateTimeInput(attrs={
			'class': 'form-control datetimepicker-input',
			'placeholder' : 'When will the even start?',
		})
	)
	eventsName = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
		"placeholder" : "What events?",
		"required" : True
	}))
	location = forms.CharField(widget=forms.TextInput(attrs={
		"class" : "form-control",
		"placeholder" : "Where will the even be held?",
		"required" : True,
	}))
	category = forms.CharField(widget=forms.TextInput(attrs={ 
		"class" : "form-control",
		"placeholder" : "What category is it?",
		"required" : True,
	}))
	class Meta:
		model = Schedule
		fields = '__all__'