from django.shortcuts import render, redirect
from .forms import addSchedule
from .models import Schedule
from django.utils import timezone
import datetime

# Create your views here.
def home(request):
	return render(request, 'Home.html')
	
def blog(request):
	return render(request, 'Blog.html')

def blog_1(request):
	return render(request, 'Blog1.html')
	
def scheduleForm(request):
    form = addSchedule()
    if request.method == "POST":
        form = addSchedule(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:schedule')

    response = {
        'form' : form,
        }
    return render(request, 'ScheduleForm.html', response)

def schedule(request):
    schedules = Schedule.objects.order_by("time")
    response = {
        'schedules' : schedules,
        }
    return render(request, 'Schedule.html', response)

def delete_schedule(request):
    if request.method == "POST":
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
    
    return redirect('homepage:schedule')

	
